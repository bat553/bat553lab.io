function replacementCitation(){
    fetch("https://api.quotable.io/random")
        .then(function (response) {
            response.json().then(function (data){
                document.getElementById("citation").innerHTML = data['content']
                document.getElementById("auteur").innerHTML = data['author']
                document.getElementById("date").innerHTML = data['dateAdded']
            })
        })
}

function newCitation(){
      // Create a request variable and assign a new XMLHttpRequest object to it.
      const request = new XMLHttpRequest()

      // Open a new connection, using the GET request on the URL endpoint
      request.open('GET', 'https://elite.bdebco.fr/citation', true)
      
      request.onload = function() {
        // Begin accessing JSON data here
        const data = JSON.parse(this.response)['citation']

        document.getElementById("citation").innerHTML = data['citation']
        document.getElementById("auteur").innerHTML = data['auteur']
        document.getElementById("date").innerHTML = data['date']
  
      }

      request.onerror = function(){
        document.getElementById("citation").innerHTML = "Houston, we have a problem."
        document.getElementById("auteur").innerHTML = "Le serveur ne répond plus."
        document.getElementById("date").innerHTML = "Les citations de l'&Sigma;lite sont indisponible."
      }

      // Send request
      request.send()

  }
